# -*- coding: utf-8 -*-
"""
Created on Fri Sep 10 17:19:56 2021

@author: MikeSha
"""

import numpy as np
import time

# Algorithm (1), repeatedly multiplying random vector b by A and normalizing the result
def power_iteration(A, trueEV, b=None):
    if b is None:
        b=np.random.randn(A.shape[0], 1)
    estimatedEig = []
    while np.abs(trueEV - np.mean(A@b/b)) > 1e-10:
        b = A @ b
        b /= np.max(np.abs(b))
        # Instead of computing Rayleigh quotient, just compute mean
        estimatedEig.append(np.mean(A @ b / b))
    return np.array(estimatedEig)

#  Algorithm (3)
def power_iteration_with_sq(A, trueEV, b=None):
    if b is None:
        b = np.random.randn(A.shape[0], 1)
    estimatedEig = []
    C = np.copy(A)
    Ab = A @ b
    while np.abs(trueEV - np.mean(C@Ab/Ab)) > 1e-10:
        # Square A at each step and normalize
        A = A @ A
        A /= np.max(np.abs(A))
        Ab = A @ b
        estimatedEig.append(np.mean(C @ Ab / Ab))
    return np.array(estimatedEig)

def test(dim=100, iters=300):
    piIters = []
    sqIters = []

    # Randomly generate real, symmetric matrices
    As = np.random.randn(iters, 100, 100)
    # Well-known that real symmetric matrices have real eigenvalues
    As += As.transpose((0, 2, 1))
    # Pf: Ax = lx
    # lx^dagger x = x^dagger (lx) = x^dagger(Ax) = (A^dagger x)^dagger x
    # = (Ax)^dagger x = l^dagger x^dagger x -> l = l^dagger


    # Evaluate ground truth largest (in magnitude) eigenvalue
    start = time.time()
    trueEVs = []
    for A in As:
        trueEVals = np.linalg.eigvals(A)
        # Add the eigenvalue
        trueEVs.append(trueEVals[np.argmax(np.abs(trueEVals))])
    print(time.time()-start)

    start = time.time()
    for i, A in enumerate(As):
        piEE = power_iteration(A, trueEVs[i])
        piIters.append(len(piEE))
    print(time.time()-start)
    start = time.time()
    for i, A in enumerate(As):
        sqEE = power_iteration_with_sq(A, trueEVs[i])
        sqIters.append(len(sqEE))
    print(time.time()-start)
    return piIters, sqIters
piIters, sqIters = test()
#%%
import matplotlib.pyplot as plt
plt.figure()
plt.hist(np.round(np.log2(piIters)), bins=9, )
plt.title('Power iteration')
plt.xlabel('$\log_2$ iterations')
plt.ylabel('count')
plt.savefig('pi.svg')
plt.figure()

plt.title('Power iteration with exponentiation')
plt.xlabel('iterations')
plt.ylabel('count')
plt.hist(sqIters, bins=9)
plt.savefig('pie.svg')