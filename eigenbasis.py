# -*- coding: utf-8 -*-
"""
Created on Fri Sep 10 17:19:56 2021

@author: MikeSha
"""

import numpy as np
import time

#  Algorithm (3)
def A20(A, power=25):
    for i in range(power):
        # Square A at each step and normalize
        A = A @ A
        A /= np.max(np.abs(A))
    return A

def gram_schmidt(A, basis=None, k=20):
    # Start with standard basis
    if basis is None:
        basis = [None for i in range(k)]
    evals = np.zeros(k, dtype=np.complex64)
    for i in range(k):
        Ahigh = A20(A)
        ei = np.random.randn(Ahigh.shape[0], 1) + np.random.randn(Ahigh.shape[0], 1) * 1j
        # Project out eigenvector of next largest eigenvalue
        eiTemp = Ahigh @ ei
        evals[i] = np.mean(A @ eiTemp / eiTemp)
        ei = eiTemp / np.linalg.norm(eiTemp)
        basis[i] = ei
        A -= evals[i] * ei.reshape((-1, 1)) @ np.conj(ei.reshape((1,-1)))

    return evals, basis


def test(dim=100, iters=300, k=2):
    eigVals = []

    # Randomly generate real, symmetric matrices
    As = np.random.randn(iters, dim, dim) + np.random.randn(iters, dim, dim) * 1j
    # Well-known that Hermitian matrices have real eigenvalues
    As += As.transpose((0, 2, 1)).conjugate()
    # Pf: Ax = lx
    # lx^dagger x = x^dagger (lx) = x^dagger(Ax) = (A^dagger x)^dagger x
    # = (Ax)^dagger x = l^dagger x^dagger x -> l = l^dagger


    # Evaluate ground truth largest (in magnitude) eigenvalue
    start = time.time()
    trueEigvals = []
    trueEigvecs = []
    for A in As:
        eigenvals, eigenvecs = np.linalg.eigh(A)
        es = list(zip(eigenvals, eigenvecs))
        es.sort(key=lambda x: np.abs(x[0]), reverse=True)
        eigenvals, eigenvecs = (*zip(*es[:k]),)
        # Add the eigenvalue
        trueEigvals.append(eigenvals)
        trueEigvecs.append(eigenvecs)
    print(time.time()-start)
    trueEigvals = np.array(trueEigvals)
    trueEigvecs = np.array(trueEigvecs)

    start = time.time()
    testEigvals = []
    testEigvecs = []
    for i, A in enumerate(As):
        eigenvals, eigenvecs = gram_schmidt(A, k=k)
        testEigvals.append(eigenvals)
        testEigvecs.append(eigenvecs)
    print(time.time()-start)
    testEigvals = np.array(testEigvals)
    testEigvecs = np.array(testEigvecs)
    return trueEigvals - testEigvals

test()
#%%
# import matplotlib.pyplot as plt
# plt.figure()
# plt.hist(piIters, bins=50, )
# plt.title('Power iteration')
# plt.xlabel('iterations')
# plt.ylabel('count')
# plt.savefig('pi.svg')
# plt.figure()

# plt.title('Power iteration with exponentiation')
# plt.xlabel('iterations')
# plt.ylabel('count')
# plt.hist(sqIters, bins=8)
# plt.savefig('pie.svg')